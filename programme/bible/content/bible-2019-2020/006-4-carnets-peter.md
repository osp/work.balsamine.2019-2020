<!-- 4. Les carnets de Peter -->

<div class="carnets-peter" markdown=true>


## 4. {: .chapt-chiffre .push-down}

## Les carnets de Peter
### Théâtre du Tilleul

3.01 → 5.01 <br/>à 15h
{: .dates}

Théâtre d’ombre et de musique
{: .type}

Les représentations sont suivies des ateliers de La Bibliothèque rêvée de Peter.
{: .push-down .type}


Peter, 87 ans, tente de se souvenir de son enfance : l'Allemagne, Munich 1936, les défilés militaires, les drapeaux, l'émigration en Amérique...Il se souvient , qu'un jour, seul dans la grande bibliothèque de son père, il décide d'inventer les étranges aventures d'un petit garçon nommé Donald, un petit garçon rêveur qui lui ressemble énormément sauf que...
{: .push-down}

Avec
:   Carine Ermans, Carlo Ferrante, Sylvain Geoffray, Alain Gilbert

Conception du spectacle
:   Carine Ermans et Sylvain Geoffray

Mise en scène
:   Sabine Durand

Conseil dramaturgique
:   Louis-Dominique Lavigne

Musique
:   Alain Gilbert

Lumière
:   Mark Elst

Régie
:   Thomas Lescart

Scénographie
:   Pierre-François Limbosh et Alexandre Obolensky

Peintures
:   Alexandre et Eugénie Obolensky aidés de Malgorzota Dzierzawska et Tancrède de Ghellinck

Costumes
:   Silvia Hasenclever

Travail du mouvement
:   Isabelle Lamouline <br/>

Images animées
:   Patrick Theunen et Graphoui

Accessoires
:   Amalgames

Production
:   Mark Elst

Coproduction
:   Théâtre la Balsamine

Soutiens  
:   Théâtre La montagne magique, CDWEJ, Maison des Cultures et de la Cohésion Sociale de Molenbeek-Saint-Jean


Le Théâtre du Tilleul est en résidence artistique et administrative au Théâtre la Balsamine.
{.production}

</div>
