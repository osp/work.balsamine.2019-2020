<!-- 3. George Kaplan de Frédéric Sonntag -->
<div class="georges-kaplan" markdown=true>



## 3. {: .chapt-chiffre .push-down}

## George Kaplan
de Frédéric Sonntag
{: .type}

### Collectif RZ1GK

26.11 → 4.12 <br/>à 20h30
{: .dates}

Création théâtre
{: .type .push-down}

Quel est le lien entre un groupe activiste clandestin en pleine crise, une équipe de scénaristes en quête de nouveaux concepts pour un projet de série télé et un gouvernement invisible aux prises avec un danger menaçant la sécurité intérieure du pays ? Un seul nom : George Kaplan. Le collectif RZ1GK questionne en endossant ces figures multiples de la paranoïa, la mince frontière qui sépare le réel de la fiction.


Partition théâtrale ludique et pleine d’humour, *George Kaplan* dissèque 3 sphères d’influence différentes que sont la militance, l’écriture et le pouvoir pour mieux en révéler les dysfonctionnements.
{: .push-down}

Avec 
:   George Cagna, George Cheverry, George Dehasseler, George Garnier-Fourniguet, George Texier

Création lumières 
:   Rémy Urbain

Production déléguée 
:   Théâtre la Balsamine

Coproductions 
:   Théâtre la Balsamine, La Coop asbl et Shelter Prod

Soutiens 
:   taxshelter.be, ING, Tax-shelter du gouvernement fédéral belge, Buktapaktop, Epongerie


</div<
