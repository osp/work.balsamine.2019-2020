<!-- Les incubations  -->
<div class="incubations" markdown=true>

# Les incubations

Un espace et du temps donnés à des artistes pour affiner, forger un projet. Moments de partage et d'expérimentations physiques et métaphysiques. Certaines incubations seront ouvertes aux publics.

## Au pied des montagnes
### Une tribu collectif
Théâtre d’ombres – jeune public
{: .type}

Résidences artistiques
{: .type}

## Je suis venu voir les gens danser autour de moi ...
### Louis Labadens/ Anna Ten
Danse – Résidence artistique et technique
{: .type}

## Forces
### Leslie Mannès
Danse – résidence artistique et technique
{: .type}

## La prisonnière des Sargasses
### Judith Ribardière
Théâtre – Résidence artistique et technique
{: .type}

## La bande de la lande
### Nelly Latour
Théâtre – Résidence artistique
{: .type}

## The violin Goat
### Anna Nilsson - Petri Dish
Cirque – Résidence artistique
{: .type}

## Materia matrix
### Oriane Varak /notch company  

Danse – Résidences artistiques
{: .type}

## Bâtard Festival
Résidences artistiques et techniques pour les artistes du festival
{: .type}

</div>
