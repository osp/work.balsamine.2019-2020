<!-- ça tourne  -->
<div class="ca-tourne" markdown=true>


# Ça tourne
Vous avez vu naître ces créations à la Balsa, elles continuent leur chemin et ont même des prix !

## i-clit
### Mercedes Dassy
créé en février 2018
{: .type}

2018
:   Prix Jo Dekmine du Théâtre des Doms, Avignon (Fr)<br/>

2018
:   Nomination aux prix de la critique « Meilleur spectacle de danse »<br/>

juin 2019
:   Festival Strange Love, Anvers (Be)<br/>

novembre 2019
:   Théâtre de l'Ancre, Charleroi (Be)<br/>

février 2020
:   Le Central, Centre Culturel La Louvière (Be)<br/>

février 2020
:   Mars - Mons arts de la scène (Be)

## Etna
### Thi-Mai Nguyen
créé en février 2018
{: .type}

2018
:   Prix de la critique « Meilleur spectacle de danse »<br/>

juillet 2019
:   Dansand Festival, Ostende (Be)

## As a Mother of Fact
### Oriane Varak / notch company
créé en juin 2018
{: .type}

juin 2020
:   Atelier 210, Bruxelles (Be)

## Où suis-je ? Qu’ai-je fait ?
### Pauline D’Ollone
créé en novembre 2018
{: .type}

octobre 2019
:   Atelier 210, Bruxelles (Be)

## Les carnets de Peter
### Le Théâtre du Tilleul
créé en décembre 2018
{: .type}

août 2019
:   Rencontres de Huy (Be)<br/>

novembre 2019
:   Théâtre de la Montagne magique, Bruxelles (Be)<br/>

décembre 2019
:   Théâtre Jean Vilar, Louvain-la-Neuve (Be)<br/>

janvier 2020
:   Théâtre la Balsamine, Bruxelles (Be)<br/>

janvier 2020
:   Service culturel de la ville de Maromme (Fr)<br/>

janvier 2020
:   Foyer culturel de Saint-Ghislain (Be)<br/>

février 2020
:   Centre Culturel de Braine-l'Alleud (Be)<br/>

février 2020
:   Centre Culturel d'Ottignies + Centre Culturel du Brabant Wallon (Be)<br/>

mars 2020
:   Centre Culturel de Tubize 5Be)<br/>

mars 2020
:   Centre dramatique Pierre de Lune à la Maison de la Culture et de la Cohésion sociale de Molenbeek Saint Jean (Be)<br/>

mars 2020
:   Centre Culturel de Waterloo (Be)<br/>

avril 2020
:   Centre Culturel Les Chiroux de Liège- Cité Miroir (Be)

## Cœur Obèse
### Amandine Laval
créé en mars 2019
{: .type}

novembre 2019
:   Théâtre de l'Ancre, Charleroi (Be)<br/>

février 2020
:   Mars – Mons arts de la scène (Be)

## Ludum
### Anton Lachky
créé en avril 2019
{: .type}

octobre 2019
:   Bratislava v pohybe (SK)<br/>

octobre 2019
:   Théâtre Marni, Bruxelles (Be)<br/>

novembre 2019
:   Ainsi Theater, Maastricht (NL)<br/>

avril 2020
:   Gessi Zurich / Step festival (CH)<br/>

mai 2020
:   Mars - Mons arts de la scène (Be)<br/>

mai 2020
:   Théâtre Nebia, Biel/Bienne/Step festival (CH)<br/>

mai 2020
:   Theater Phönix, Steckborn/Step festival (CH)<br/>

mai 2020
:   Burghof, Lörrach/Step festival (CH)<br/>

mai 2020
:   Château Rouge, Annemasse/Step festival (CH)<br/>

mai 2020
:   Château Rouge, Annemasse ( option)/Step festival (CH)

</div>
