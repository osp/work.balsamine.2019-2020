<div class="extras push" markdown=true>

# Autour <br/>des spectacles

Défendre la nécessité de l’Art pour l’Art mais le rendre accessible au plus grand nombre est une utopie qui nous anime et dont nous entendons prendre responsabilité. Le Théâtre la Balsamine est une maison de création qui abrite et accompagne des artistes confrontés quotidiennement à la nécessité de s’interroger sur le sens, sur le cadre dans lequel ils
exercent leur pratique, sur le style qu’ils élaborent et sur les valeurs qu’ils représentent. Quand un artiste entreprend de donner forme à sa perception, à sa compréhension du monde et à ce qu’il souhaite énoncer, il n’a pas pour projet d’être inoffensif, de conforter le consensus. La création contemporaine est une forme mouvante et en perpétuel questionnement, qui renvoie le spectateur à sa capacité à créer du sens et à vivre son rapport au monde. Elle nous interpelle sur notre propre créativité et sur notre capacité à exprimer qui nous sommes. Elle interroge les normes, y compris celles du goût. C’est un questionnement précieux que nous entendons partager avec le grand public.

## Les aftershows

À l’issue de toutes les deuxièmes représentations des créations, la Balsamine vous propose une rencontre avec l’équipe artistique. Un moment privilégié pour débattre et échanger en toute convivialité. Petite particularité, cette rencontre est menée par un autre artiste de la saison.

## Le public scolaire

Si le théâtre classique offre des repères rassurants, il ne traduit pas toujours le caractère hybride et complexe du monde dans lequel nous sommes amenés à évoluer.

Emmener les jeunes au théâtre pour y voir de la création contemporaine est une occasion de les familiariser avec des codes qui leurs sont inhabituels, les amener à déchiffrer des représentations de l’aujourd’hui.

Faire vivre l’expérience du contemporain aux jeunes peut être aussi une façon de les aider à démonter un préjugé trop souvent répandu selon lequel l’art théâtral serait une discipline poussiéreuse et archaïque, étrangère à leur culture et à leurs questionnements, qui ne leur appartiendrait pas, qui leur serait inaccessible.

N’hésitez donc pas, si un spectacle de notre saison vous donnait l’envie de mettre en relation vos étudiants avec la pratique artistique, à nous faire part de vos idées.

Nous organisons régulièrement des dynamiques d’échanges entre étudiants et professionnels, sous différentes formes en fonction des projets spécifiques. Pour exemples: captations vidéo, stages d’observation, interviews d’artistes, séances de croquis, ateliers créatifs…

Contact conseils et organisation des activités pédagogiques
:  Noemi Tiberghien
:  +32 2 737 70 18
:  noemi.tiberghien@balsamine.be

# Les extras
## Les Balsatoiles

Dès 14h.
Le temps de 7 mercredis après-midis, la Balsa se transforme en cinéma de quartier. Ces projections gratuites de films d’animation sont des moments privilégiés de partage. Chaque film est précédé d’un atelier créatif en lien avec les thématiques abordées par l’œuvre. Entrée libre mais réservation souhaitée !  
{: .push-down}

</div>
