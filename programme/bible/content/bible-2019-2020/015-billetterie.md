<div class="info-pratique billetterie" markdown=true>



# Billetterie {: .push}

![](../../img/tarif-mecene.svg)

:  Être mécène, c’est participer à la pérennité des fondamentaux de la Balsamine que sont l’échange et le dialogue, la création et l’enjeu artistique et expérimental.
C’est nous aider à soutenir la création artistique. C’est reconnaître et accompagner une politique qui soutient les jeunes artistes.
C’est vous associer au pari de la jeunesse et de la rencontre des savoirs issus de différentes disciplines.
C’est une opportunité singulière de nous permettre de poursuivre une politique tarifaire accessible à tous.
{: .footnote }

:  <br/>

![](../../img/tarif-plein.svg)


![](../../img/tarif-reduit.svg)

:  Étudiants, + de 60 ans, demandeurs d’emploi, professionnels du spectacle, schaerbeekois
{: .footnote }

:  <br/>

![](../../img/tarif-enfant.svg)


![](../../img/tarif-etu-art.svg)


![](../../img/tarif-peter.svg)


![](../../img/tarif-noel.svg)

![](../../img/art27-arsene.svg)


![](../../img/tarif-groupe.svg)

![](../../img/tarif-groupe2.svg)

:  Le paiement des réservations de groupe doit s’effectuer 10 jours avant la date de représentation choisie
{: .footnote }

![](../../img/tarif-united.svg)

## Réservations {: .plus-haut}

### Par téléphone

Le bureau des réservations est joignable au [+32 2 735 64 68](tel:+3227356468). Du lundi au vendredi, de 14h30 à 18h. En dehors de ces heures et le week-end, un répondeur prend vos réservations.

### Par e‑mail

Vous pouvez envoyer un e‑mail à l’adresse [reservation@balsamine.be](mailto:reservation@balsamine.be) en précisant votre nom et votre prénom, le nom du spectacle, la date de représentation, le(s) tarif(s) dont vous bénéficiez et le nombre de places souhaitées.

### Par le site internet

Accessible 24h/24.

## Moyens de paiement et retrait des places

### Préventes

Pour bénéficier du tarif prévente, le paiement doit avoir lieu 48h avant la date de représentation choisie.

### Sur place

La billetterie est ouverte les soirs de représentation à partir de 19h00.

### Règle

Toute place achetée ne peut en aucun cas bénéficier d'un remboursement. Cependant, un report de place est possible sur un autre spectacle de la même saison.


</div>
