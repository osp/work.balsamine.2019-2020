<!-- 2. Ce qui vit en nous -->
<div class="ce-qui-vit" markdown=true>



## 2. {: .chapt-chiffre .push-down}

## Ce qui vit en nous
### Stéphane Arcas, <br/>Baudouin de Jaer <br/>et Martijn Dendievel

5.11 → 9.11 <br/>à 20h30
{: .dates}

Opéra de chambre en 3 actes
{: .type .push-down}

![ars](../../img/arsmusica.svg)


Au beau milieu d’un espace inconnu, un jeune homme amnésique tente de rassembler des bribes de souvenirs pour y trouver la raison de sa présence. Une infirmière le rejoint mais ne parvient pas à l’éclairer sur sa situation. Après diverses hypothèses absurdes, le rideau se lève sur la réalité : il s’agit d’une salle d’autopsie.


*Ce qui vit en nous* est une comédie chantée dans laquelle chacun des personnages pose la question de la mort et de son omniprésence dans l’esprit de tout être vivant. Tour à tour, ils dressent à leur façon le portrait de cette sensation d’« inquiétante étrangeté », comme la nommait Freud. Elle est le lieu où le quotidien se mue en ombre et où le vivant et l’inanimé entrent en collusion. Elle se loge ainsi dans tout ce qui nous est familier. Absente de nos pensées, dans le tourbillon de nos activités, la Faucheuse apparaît sans prévenir et l’on s’aperçoit, alors, qu’elle était là, depuis le début. Elle est ce qui vit en nous.
{: .push-down}


Livret, mise en scène et scénographie 
:   Stéphane Arcas

Composition
:   Baudouin de Jaer

Chef
:   Martijn Dendievel

Chanteurs et comédiens
:   Lorenzo Carola, Paul Gérimon, Nicolas Luçon, Sarah Théry

Musiciens
:   Ensemble beSides, Romy-Alice Bols (flûte), Jasper Braet (électronique), Toon Callier (guitare électrique), Fabian Coomans (clavier), Sam Faes (violoncelle), Nele Geubels (saxophone), Dejana Sekulic (violon), Jeroen Stevens (percussions), Jutta Troch (harpe)

Assistanat mise en scène
:   Cécile Chèvre 

Concept spatial et dispositif
:   Stéphane Arcas, Claude Panier et Marie Szernovicz

Lumières
:   Julie Petit-Etienne

Ingénieur du son
:   Marc Doutrepont

Captation
:   Mathieu Haessler et Sonia Ringoot

Production déléguée 
:   Théâtre la Balsamine

Coproductions
:   Théâtre la Balsamine, Ars Musica, La Coop asbl et Shelter Prod

Soutiens 
:   BLACK FLAG ASBL, NOODIK productions, Forum des compositeurs, Fédération Wallonie-Bruxelles- Service du théâtre, taxshelter.be,  ING, Tax-shelter du gouvernement fédéral belge


</div>
