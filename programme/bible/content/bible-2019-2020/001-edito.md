<!-- INTRO -->
<div class="intro" markdown=true>


# Introduction à l’histoire d’une saison, <br/>toutes les histoires d’une saison.

Comment conter l’histoire d’une saison ?
Comment résumer toutes les histoires d’une saison ?
Comment donner à voir, avant l’heure, alors que rien n’est créé ?
Comment dire ce qui, dans ces fictions, interpelle. Comment le partager, susciter la curiosité ?
Par où commencer ?
{: .intro}

> « Il faut commencer par le commencement, et le commencement <br/>de tout est le courage. Il faut dire que le courage est la vertu inaugurale du commencement. » Vladimir Jankélévitch

Le courage, c’est se jeter à l’eau.
C’est un geste d’urgence et sans explication.
C’est une insurrection de l’individu, une façon de ne pas être soumis, une manière d’éviter la propagande, de ne pas simplifier pour séduire.  
Pas un jour ne passe sans que l’on se souhaite du courage pour accomplir les tâches les plus quotidiennes, celles qui nous incombent en tant que parent, travailleur, ami, citoyen, éducateur, artisan, …
{: .intro}

Le théâtre, aussi, est un art vivant qui demande du courage. Ce temps citoyen particulier travaille à la consolidation de nos émancipations individuelles et collectives.
La scène intimide, elle se gagne, se mérite. Elle est cet espace des possibles où nos diversités trouvent nourriture à nos engagements communs.
{: .intro}

Si les nouveaux langages émeuvent et sidèrent, c’est parce qu’ils tissent les liens du futur, ils osent une forme de désobéissance face à ce qui paraissait, hier encore, indéboulonnable. Les artistes de demain sont une nouvelle espèce, tous genres confondus, qui garde en elle la possibilité de transgresser. Ils font preuve de ...
{: .intro}

Pour amorcer la rencontre, en attendant le développement des créations jusqu’à leur forme spectaculaire, nous avons composé un programme aux multiples entrées.
Vous découvrirez donc, dans ce cahier d’artistes indisciplinés, trois traversées possibles de la saison à venir :
{: .intro}


* l’une, intuitive, imagée voire poétique.
* l’autre, informative et pratique.
* la troisième (à vivre) sera la vôtre, elle demandera votre venue, votre présence physique, votre regard.



Haut les cœurs !
Belle errance, soyez intrépides !
{: .intro}


Monica Gomes
{: .author}

</div>
