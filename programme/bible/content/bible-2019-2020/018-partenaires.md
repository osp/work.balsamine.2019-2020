<div class="partenaires info-pratique" markdown=true>


# Partenaires

La Balsamine est subventionnée par la Fédération Wallonie-Bruxelles et fait partie du réseau des Scènes chorégraphiques de la Commission Communautaire française de la Région de Bruxelles-Capitale.

La Balsamine reçoit aussi le soutien de Wallonie-Bruxelles Théâtre/Danse et de Wallonie-Bruxelles International.

![](../../img/label-united-stages-txt.svg)

La Balsamine est signataire de la charte United Stages.

UNITED STAGES ce sont une cinquantaine de structures et associations culturelles qui s’engagent publiquement à défendre et promouvoir un projet de société progressiste, solidaire et respectueux des droits humains.

La création du label United Stages est consécutive à la volonté du secteur culturel d’exprimer haut et fort son soutien à toutes les personnes fragilisées par les effets des politiques inhumaines et discriminatoires. L’article 23 de notre Constitution garantit à chacun “*le droit de mener une vie conforme à la dignité humaine*”. Alors ensemble, nous refusons d’être les spectateurs passifs du déni de ce droit fondamental. Sans accès au logement, à la santé, ou à la formation nulle possibilité de participer à la vie sociale et culturelle. Luttons contre la résignation, luttons contre la haine et le mépris mais surtout luttons pour les droits essentiels de ces femmes, de ces hommes et de ces enfants, nos pairs et nos égaux !


</div>
