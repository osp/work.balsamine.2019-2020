<!-- 6. Backstage -->

<div class="backstage" markdown=true>


## 6. {: .chapt-chiffre .push-down}

##Backstage
Adaptation libre et bilingue du texte éponyme d'Oscar Van Woensel
{: .type}

### Collectif BXL WILD

12.02 → 21.02 <br/>à 20h30
{: .dates}

Création théâtre <br/>
FR / NL
{: .type .push-down}

Au Théâtre National d’une ville totalement fictive, une compagnie, réputée et grassement subventionnée, joue la centième représentation de *Hamlet* de William Shakespeare. Durant cette représentation, six figurants - francophones et néerlandophones – prennent en charge, tour à tour, tous les rôles muets de la pièce. Toute l’action se déroule dans la loge des figurants, un vendredi soir, par une froide nuit de décembre. Tandis que l’ennui et la contrariété vont grandissant, les voilà qui se mettent à boire et à saboter leurs entrées en scène, provoquant intrigues et broutilles en tous genres…


*Backstage* est le premier spectacle de BXL WILD, collectif créé par Ina Geerts et Circé Lethem. Toutes deux cherchent à créer des spectacles bilingues, tout en affirmant leur profonde identité bruxelloise, expérimentant sur scène cette dualité qu'elles vivent dans leur quotidien. Pour cette création, le collectif réunit 3 femmes et 3 hommes, francophones ou néerlandophones.
{: .push-down}

Avec  
:   Andréa Bardos, Bernard Eylenbosch, Ina Geerts, Eno Krojanker, Circé Lethem, Tomas Pevenage

Création sonore
:   Guillaume Istace

Création costumes  
:   Lieve Meeussen

Création lumière
:   Rémy Urbain

Regard extérieur
:   Vital Schraenen

Production déléguée 
:   Théâtre la Balsamine

Coproductions 
:   Théâtre la Balsamine, La Coop asbl et Shelter Prod

Soutiens 
:   Fédération Wallonie-Bruxelles –Service du théâtre, Théâtre Jardin Passion, taxshelter.be, ING et Tax-shelter du gouvernement fédéral belge


</div>
