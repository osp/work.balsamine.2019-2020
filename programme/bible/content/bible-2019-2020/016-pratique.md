<div class="info-pratique pratique" markdown=true>


# Pratique {: .push}

## Accès

Tous les chemins mènent à la Balsa.


![map71-1](../../img/map-19-20.png)

![map71-2](../../img/map-19-20-zoom.png)


La Balsamine
:   Avenue Félix Marchal, 1
:   1030 Bruxelles


Administration
:   +32&nbsp;2&nbsp;732&nbsp;96&nbsp;18
:   info@balsamine.be

Réservation
:   +32&nbsp;2&nbsp;735&nbsp;64&nbsp;68
:   reservation@balsamine.be


L’accessibilité de nos salles aux personnes à mobilité réduite est possible mais pas aisée. Lors de vos réservations, nous vous remercions de bien vouloir le notifier pour que nous puissions vous accueillir au mieux.
{: .footnote .plusbas}


## Bar et restauration

Le bar est ouvert à 19h00 chaque soir de représentation. Une petite restauration délicieuse et faite sur place avec des produits locaux et de saison vous est proposée avant chaque spectacle.

</div>
