<!-- 7. XX Time -->
<div class="xx-time blocks xx-time-0" markdown=true>

## 7. <br/>XX Time

8.03 → 4.04 <br/>
{: .dates}

Programmation <br/>en cours
{: .type}

</div>

<div class="xx-time blocks xx-time-a" markdown=true>

#### 7.a {: .chapt-chiffre .push-down}

#### March

### Nathalie Rozanes

17.03 → 18.03 <br/>à 20h30
{: .dates}

Performance
{: .type .push-down}

> « Je crois que le seul espace d’honnêteté qui nous reste est l’espace individuel. » Julia Kristeva dans *Revolt, She Said*.

En passant du stand-up à la poésie du « spoken word », d’extraits de romans autobiographiques aux carnets de voyages, ou de manifestes ecosexuels à des appels Skype, Nathalie Rozanes invite la danseuse américaine Elizabeth Ward à juxtaposer ensemble différentes formes de langages à la première personne du singulier. Sur scène, le « je » se déploie dans toute sa pluralité, rendant compte de la nature complexe et fragmentée de notre identité. En dévoilant le caractère multiple de la sienne, la performeuse abordera alors les inconforts politiques et privés ressentis depuis le début de cette recherche en mars 2017. Une traversée identitaire accompagnée en live par le musicien électronique Frédéric Altstadt.

Idée et Concept  
:   Nathalie Rozanes

Performance et creation
:   Frédéric Alstadt, Nathalie Rozanes, Elizabeth Ward

Texte
:   Nathalie Rozanes, Elizabeth Ward, Leonard Cohen, George Perec, Ben Lerner, Michael Ondaatje

Montage texte
:   Nathalie Rozanes

Son
:   Frédéric Altstadt

Chorégraphie  
:   Elizabeth Ward

Espace  
:   Simon Siegmann

Dramaturgie
:   Jellichje Reijnders

Résidences
:   workspacebrussels, Campo

Soutiens 
:   VGC, workspacebrussels, WIPCOOP / Mestizo Arts


</div>

<div class="xx-time blocks xx-time-b" markdown=true>

#### 7.b {: .chapt-chiffre .push-down}
#### In my big fireworks I play to you the final bouquet
### Les sœurs h

27.03 et 28.03 <br/>à 20h30
{: .dates}

Concert Performé
{: .type .push-down}

Dans un entre-deux flottant, trop rose et trop pailleté, deux adolescentes et un musicien naviguent à vue.
Dans cet espace hybride où se mêlent musique live, vidéo, écriture et performance,  Les sœurs h explorent le passage de l’enfance à l’âge adulte comme un plongeon dans l’inconnu.

À travers la performance, les artistes questionnent les notions de transition et de construction identitaire, caractéristiques de l’adolescence. Période d’incertitudes, de tentatives, de désirs, d’ambivalences et de contradictions. Les sœurs h s’intéressent à ce désir de trouver sa propre voie, à ce rejet instinctif des schémas préétablis. Elles traduisent cette quête d’identité bouillonnante, quasi explosive, qui n’a pas peur de sortir des sentiers battus, pour y trouver son propre sens.

Vidéos, texte 
:   Les sœurs h (Marie Henry et Isabelle Henry)

Musique live 
:   Maxime Bodson

Performance 
:   Clara Petit-Jean et Jeanne Wehrlin

Scénographie 
:   Marie Szersnovicz

Production 
:   Mortimer et les sœurs h, Perfect Shirley asbl

Soutiens
:   SACD, Ville de Lausanne, Casino Barrière Montreux, Pro Helvetia, Fondation suisse pour la culture

Partenaires 
:   La Bellone, Maison de Quartier de Chailly


</div>

<div class="xx-time blocks xx-time-c" markdown=true>

#### 7.c {: .chapt-chiffre .push-down}
#### Cœur obèse
### Amandine Laval

3.04 et 4.04 <br/>à 20h30
{: .dates}

Performance (+18 ans)
{: .type .push-down}

En ôtant sa culotte, en dézippant sa robe de soirée, en dégrafant sa lingerie sans en avoir l'air mais bien l'envie, en traquant le désir d'inconnus, en faisant jaillir leur excitation, en simulant le coup de foudre, le respect, l'amour véritable, compose-t-on finalement un spectacle comme un autre ? Une strip-teaseuse amateure, seule en scène, se décharge des fictions accumulées.


Texte et interprétation
:   Amandine Laval

Musique
:   Pierre-Alexandre Lampert

Création lumière
:   Alice De Cat

Production
:   Théâtre la Balsamine


</div>
