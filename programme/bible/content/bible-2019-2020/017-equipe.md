<div class="equipe info-pratique" markdown=true>


# Équipe {: .push}

Direction générale et artistique<br/>
:   Monica Gomes<br/>

Direction financière et administrative<br/>
:   Morgan Brunea<br/>

Coordination générale, communication et accueil compagnies<br/>
:   Fanny Arvieu<br/>

Presse, communication, promotion et relations publiques<br/>
:   Irène Polimeridis<br/>

Comédienne et romaniste - Médiation écoles et associations<br/>
:   Noemi Tiberghien<br/>

Metteur en scène et auteur - Artiste associée<br/>
:   Martine Wijckaert<br/>

Résidence artistique et administrative<br/>
:   Théâtre du Tilleul<br/>

Direction technique<br/>
:   Jef Philips<br/>

Régisseur<br/>
:   Rémy Urbain<br/>

Responsable bar<br/>
:   Franck Crabbé<br/>

Photographe de plateau associé<br/>
:   Hichem Dahes<br/>

Designers graphiques associés<br/>
:   Open Source Publishing<br/>

Comptabilité<br/>
:   Geoffroy Delhez<br/>

</div>
