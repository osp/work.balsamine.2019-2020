<!-- 5. B4 Summer -->

<div class="b4-summer" markdown=true>


## 5. {: .chapt-chiffre .push-down}

##B4<br/>summer
### Mercedes Dassy

4.02 → 8.02 <br/>à 20h30
{: .dates}

Création danse
{: .type .push-down}

Confus.e.s ? En perte de repères ? Fatigué.e.s ? Angoissé.e.s ? Ou encore, indécis.e.s mais insoumis.e.s ? Si vous ressentez à la fois cette désillusion des révolutions ainsi qu’un indomptable sentiment de révolte, bienvenu.e.s dans B4 summer - The Game. Dans cette dimension créative, il vous sera possible d’inventer de nouvelles fictions pour contrer notre système en dépérissement. *B4 summer*, c'est cette fête permanente du corps et de l’esprit qui tente de s’affranchir des modèles imposés.

Dans son précédent solo *i-clit*, la chorégraphe Mercedes Dassy explorait une nouvelle vague de féminisme ultra-connectée et ultra-sexuée, et questionnait son pouvoir ambivalent d’affranchissement et d’oppression. En s’inspirant de représentantes contemporaines tant réelles que fictionnelles pour interroger notre (in)capacité à agir et à nous engager, *B4 summer* s’inscrit naturellement comme suite logique. Mais par où commencer quand tout est à défaire et à refaire ? Top chrono. Réponses attendues avant l'été.
{: .push-down}

Chorégraphie et interprétation
:   Mercedes Dassy

Dramaturgie, regard extérieur
:   Sabine Cmelniski

Création sonore
:   Clément Braive

Scénographie et création lumière
:   Caroline Mathieu

Costumes, scénographie
:   Justine Denos

Collaboration dramaturgique
:   Jill De Muelenaere

Diffusion
:   France Morin & Jill De Muelenaere | Arts Management Agency (AMA)

Production déléguée 
:   Théâtre la Balsamine / Arts Management Agency <br/>

Coproductions 
:   Théâtre de Liège, Théâtre la Balsamine, Kunstencentrum Vooruit, Charleroi danse - Centre chorégraphique de Wallonie-Bruxelles, Mars-Mons arts de la scène, La Coop asbl et Shelter Prod

Soutiens 
:   Fédération Wallonie-Bruxelles - Service de la danse, [e]utopia, Bamp, taxshelter.be, ING et Tax-shelter du gouvernement fédéral belge


</div>
