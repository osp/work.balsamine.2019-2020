<!-- Partenariats. L’école des maîtres  -->
<div class="partenariats push partenariats-0" markdown=true>

# Les partenariats {: .push}
</div>
<div class="partenariats partenariats-ecole" markdown=true>
## L’école des maîtres

### Angelica Liddell

23.09 <br/>à 20h00
{: .dates}

Entrée libre – réservation souhaitée
{: .type}

</div>
<div class="partenariats partenariats-noel" markdown=true>
## Festival Noël au théâtre

26.12 → 30.12
{: .dates}

Programmation en cours
{: .type}

</div>
<div class="partenariats partenariats-festivalup" markdown=true>

## Festival UP

19.03 → 29.03
{: .dates}

Biennale Internationale de Cirque de l’Espace Catastrophe
{: .type}

Programmation en cours
{: .type}

> Infos & Tickets : www.upfestival.be


</div>
<div class="partenariats partenariats-kunst" markdown=true>
## Kunstenfestivaldesarts

8.05 → 30.05
{: .dates}

Programmation en cours
{: .type}

> Infos & Tickets : www.kunstenfestivaldesarts.be

</div>
