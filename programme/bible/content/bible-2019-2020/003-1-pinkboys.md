<!-- 1. Oser rompre / pink boy -->
<div class="oser-rompre" markdown=true>



## 1. {: .chapt-chiffre .push-down}

## Pink<br/> boys <br/>and old ladies
### Kosmocompany

28.09 → 5.10 <br/>à 20h30
{: .dates}

Création théâtre
{: .type .push-down}

Norman, qui n’est pas normand, est un petit garçon assez banal, mais Norman aime porter des robes. La sœur du père de Norman trouve ce comportement limite limite - elle double toujours les mots quand elle est gênée. La mère de Norman aimerait parfois trépaner son fils pour voir ce qui cloche à l’intérieur, ce qui est radical mais pas le plus pratique. Sa grand-mère maternelle - et pourquoi pas une cure d’hormones ? - semble être la plus concrète mais Norman ne veut pas changer de sexe, Norman veut juste se sentir bien, dans cet entre-deux, dans cet espace indéfini dans lequel il évolue, et qui ne porte pas de nom. Le père de Norman, enfin, aimerait s’exprimer, mais ce qu’il dit est inintelligible. Un jour, il décide d’accompagner son fils en robe à l’école…


Tiré d’un fait divers réel, *Pink boys and old ladies* déploie une fresque familiale impressionniste et caustique, où chacun, empêtré dans cette situation délicate, exprime mal son malaise. Si dans cette famille on parle beaucoup, on parle surtout de rien, parce qu’on ne veut pas parler de tout.
{: .push-down}


Mise en scène
:   Clément Thirion 

Écriture et dramaturgie
:   Marie Henry 

Interprétation
:   Gwen Berrou, Lucas Meister, Clément Thirion, Simon Thomas, Mélodie Valemberg, Mélanie Zucconi

Assistante mise en scène
:   Déborah Marchal 

Musique
:   Thomas Turine 

Scénographie, lumières et costumes
:   Saskia Louwaard et Katrijn Baeten 

Vidéo et photographie
:   Julien Stroïnovsky

Régie générale et direction technique
:   Christophe Van Hove <br/>

Accompagnement et diffusion
:   BLOOM Project /Stéphanie Barboteau 

Production déléguée 
:   MARS, Mons arts de la Scène

Coproductions 
:   Kosmocompany, Théâtre de Liège, Théâtre la Balsamine, Maison de la Culture de Tournai/maison de création, La Coop asbl et Shelter Prod

Soutiens 
:   Fédération Wallonie-Bruxelles- Service du théâtre, taxshelter.be,  ING, Tax-shelter du gouvernement fédéral belge


</div>
