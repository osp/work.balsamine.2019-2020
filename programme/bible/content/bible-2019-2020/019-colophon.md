<div class="start-info"></div>
<div class="info-pratique colophon" markdown=true>

#Colophon

Dessins et graphisme
:   Open Source Publishing, Ludi, Sarah, Gijs, Pierre<br/>

Outils et programmes
:   html2print, Inkscape, Gimp, Scribus, git, gitlab, etherpad, GNU/Linux<br/>

Typographies
:   Ume Vertical, Ume Gothic Bold, Ume Mincho<br/>

Fichiers de mise en page disponibles sous licence art libre sur http://osp.kitchen /work/balsamine.2019-2020/
{: .production}

Ce programme est composé à partir d'images choisies par les artistes de la saison et en partie extraites de leurs dossiers préliminaires. Sur cette base, OSP recherche des visuels similaires basées sur la chromie et la composition. Toutes ces images sont sous des licences qui permettent leur utilisation dans ce contexte. Voir la liste complète des crédits sur https://balsamine.be/saison-19-20/credits-images.html
{: .production}

Impression
:   Imprimerie Gillis, Bruxelles<br/>

Éditeur responsable
:   Monica Gomes, Avenue Félix Marchal, 1 - 1030 Bruxelles<br/>
